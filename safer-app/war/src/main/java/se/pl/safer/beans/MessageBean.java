package se.pl.safer.beans;

import se.pl.safer.domain.Message;

/**
 * A bean that hold's the data for @Object Message.
 * @author Team Safebroker
 */
public class MessageBean {

	private long id;
	private String message;
	
	public void copyMessageValuesToBean(Message message){
		setId(message.getId());
		setMessage(message.getMessage());
	}
	
	public void copyBeanValuesToMessage(Message message){
		message.setId(getId());
		message.setMessage(getMessage());
	}
	

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "MessageBean [message=" + message + "]";
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
}
