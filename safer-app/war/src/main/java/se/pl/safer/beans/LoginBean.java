package se.pl.safer.beans;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * A bean containing the three variables used in the login process.
 * @NotEmpty 
 * @author Team Safebroker
 */
public class LoginBean {
	
	@NotEmpty
	private String username;
	@NotEmpty
	private String password;
	@NotEmpty
	private String otp;

	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}


	@Override
	public String toString() {
		return "LoginBean [username=" + username + ", password=" + password
				+ ", otp=" + otp + "]";
	}

	

}
