package se.pl.safer.controller;

import java.util.logging.Logger;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.pl.safer.beans.MessageBean;
import se.pl.safer.domain.Message;
import se.pl.safer.services.MessageService;
import se.pl.safer.util.ValidateMessage;


/**
 * Controller to handle the created messages in the app "Safer". Full CRUD.
 * Validates messages.
 * @Inject MessageService
 * @author Team Safebroker
 */
@Controller
@RequestMapping()
public class MessageController {

	private static Logger log = Logger.getLogger(MessageController.class
			.getName());

	@Inject
	private MessageService service;

	@RequestMapping(value = "/messageBoard/{id}.html", method = RequestMethod.GET)
	public ModelAndView showMessage(@PathVariable long id) {
		log.info("showMessage, GET");
		log.info("showMessage, ID="+id);

		if (!LoginController.isAuthorizedUSER()) {
			log.info("The user is no longer authorized. Redirect back to index page.");
			return LoginController.abortLoginProcess();
		}
		
		ModelAndView modelAndView = new ModelAndView("messageBoard");
		MessageBean messageBean = new MessageBean();
		
		if(id > 0){
			log.info("edit message. Get the message from db and fill the bean with data.");
			Message message = getService().getMessage(id);
			messageBean.copyMessageValuesToBean(message);
		} else {
			log.info("new message");
		}
		
			modelAndView.addObject("allMessages", service.getAllMessages());
			modelAndView.addObject("messageBean", messageBean);

			return modelAndView;
	}

	@RequestMapping(value = "/messageBoard/{id}.html", method = RequestMethod.POST)
	public ModelAndView postNewMessage(MessageBean bean,
			BindingResult bindingResult) {
		log.info("Message: " + bean.getMessage().toString());

		if (!LoginController.isAuthorizedUSER()) {
			log.info("The user is no longer authorized. Redirect back to index page.");
			return LoginController.abortLoginProcess();
		}

		if(bean.getId() > 0) {
			Message message = service.getMessage(bean.getId());	
			bean.copyBeanValuesToMessage(message);
			message.setAuthor(LoginController.getAuthorizedUSERNAME());
			if(ValidateMessage.validate(message)){
			service.updateMessage(message);
			}
		} else {
			log.info("Message to post: " + bean.getMessage().toString());
			Message message = new Message(bean.getMessage());
			message.setAuthor(LoginController.getAuthorizedUSERNAME());
			if(ValidateMessage.validate(message)){
			service.createMessage(message);
			}
		}

		return new ModelAndView("redirect:/messageBoard/0.html");
	}

	@RequestMapping(value = "/deleteMessage/{id}.html", method = RequestMethod.GET)
	public ModelAndView deleteMessage(@PathVariable long id) {
		log.info("deleteMessage, GET");

		if (!LoginController.isAuthorizedUSER()) {
			log.info("The user is no longer authorized. Redirect back to index page.");
			return LoginController.abortLoginProcess();
		}
		if (!getService().isMessageInDb(id)) {
			log.info("Message don't exist. Redirect back to index page.");
			LoginController.setAuthorizedUSER(false);
			return LoginController.abortLoginProcess();
		}
		
		getService().removeMessage(id);
		log.info("Delete message with id= " + id);

		return new ModelAndView("redirect:/messageBoard/0.html");
	}

	public MessageService getService() {
		return service;
	}
	public void setService(MessageService service) {
		this.service = service;
	}
}
