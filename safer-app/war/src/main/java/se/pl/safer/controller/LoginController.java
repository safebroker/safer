package se.pl.safer.controller;

import java.util.logging.Logger;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.pl.safer.beans.LoginBean;
import se.pl.safer.domain.User;
import se.pl.safer.services.UserService;
import se.pl.safer.services.YubikeyService;
import se.pl.safer.services.mock.YubikeyServiceMock;

/**
 * Controller to handle the login function.  
 * YubikeyService handles the logic for the yubikey.
 * UserService validates the users password and username.
 * @Inject UserService
 * @Inject YubikeyService
 * @Inject YubikeyServiceMock (ONLY FOR TESTING!)
 * @author Team Safebroker
 */
@Controller
@RequestMapping("/index.html")
public class LoginController {

	private static Logger log = Logger.getLogger(LoginController.class.getName());

	@Inject
	UserService userService;
	
	@Inject
	YubikeyService yubikeyService;
	
//	@Inject
//	YubikeyServiceMock yubikeyService;

	private static boolean authorizedUSER = false; 
	private static String authorizedUSERNAME;
	private boolean loginSuccess = false;
	private boolean userIsValid = false;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView login() {
		log.info("index login GET");
		setAuthorizedUSER(false);

		ModelAndView mav = new ModelAndView("index");
		mav.addObject("loginBean", new LoginBean());

		return mav;
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView postLoginData(@Validated LoginBean loginBean,
			BindingResult errors) {
		log.info("postLoginData POST");

		//Check if the bean contains any errors. If it does, The login will cancel.	
		if(errors.hasErrors()) {
			log.info("The LoginBean has errors");
			setAuthorizedUSER(false);
			return abortLoginProcess();
		}

		// Call DB to find a correct and matching password.
		userIsValid = getUserService().userIsValid(loginBean.getUsername(), loginBean.getPassword());

		// Verify OTP from the Yubikey
		if(userIsValid) {
			log.info("User's password or username was true and made it possible to continue the login. userIsValid value = "+ userIsValid);
			User validUser = getUserService().getValidUser();
			loginSuccess = yubikeyService.loginWithYubiKey(loginBean.getOtp(), validUser);
		
		} else {
			log.info("User's password or username was false and made it impossible to login. userIsValid value = "+ userIsValid);
			setAuthorizedUSER(false);
			return abortLoginProcess();
		}

		// Make sure both passwords and yubikey is valid.
		if (loginSuccess && userIsValid) {
			log.info("Both password and yubikey is valid. Login is OK. Sending the user into the secret message board.");
			setAuthorizedUSER(true);
			setAuthorizedUSERNAME(loginBean.getUsername());
			return new ModelAndView("redirect:/messageBoard/0.html");
		
		} else {
			log.info("Password or yubikey is NOT valid. Login is cancelled.");
			setAuthorizedUSER(false);
			return abortLoginProcess();
		}
	}
	
	public static ModelAndView abortLoginProcess() {
		return new ModelAndView("redirect:/index.html"); 
	}
	
	public static String getAuthorizedUSERNAME() {
		return authorizedUSERNAME;
	}

	public static void setAuthorizedUSERNAME(String authorizedUSERNAME) {
		LoginController.authorizedUSERNAME = authorizedUSERNAME;
	}

	public static boolean isAuthorizedUSER() {
		return authorizedUSER;
	}

	public static void setAuthorizedUSER(boolean authorizedUSER) {
		LoginController.authorizedUSER = authorizedUSER;
	}

	public UserService getUserService() {
		return userService;
	}
}
