package se.pl.safer.util;

import java.util.logging.Logger;
import se.pl.safer.domain.Message;

/**
 * Validate class for message.
 * Uses regex and other validation.
 * @author Team Safebroker
 *
 */
public class ValidateMessage {

	private static Logger log = Logger.getLogger(ValidateMessage.class.getName());
	
	public static boolean validate(Message message) {
		if(message.getMessage().isEmpty()){
			return false;
		} 
		if(message.getAuthor().isEmpty()) {
			return false;
		}
		if(message.getMessage().length() > 50){
			return false;
		}
		if(message.getMessage().substring(0, 1).contains(" ")){
			return false;
		}
		
		
		return validateWithRegex(message.getMessage());
	}

	public static boolean validateWithRegex(String message) {
		boolean matches = message.matches("^[a-zA-Z0-9_ ]+$");
		log.info("Message regex = " + matches);
		return matches;
	}
	
	
	
}
