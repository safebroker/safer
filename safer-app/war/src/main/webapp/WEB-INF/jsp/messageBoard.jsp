<%@page contentType="text/html; charset=UTF-8" errorPage="error.jsp"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/style/common.css" />
<title>Safer - Message board</title>
</head>
<body>

	<div class="messageBoard">
		<div class="loginForm">

			<table class="messageTable">

				<c:forEach items="${allMessages}" var="message">

					<tr>
						<th align="center">Message ID:</th>
						<th align="center">Author:</th>
						<th align="center">Content:</th>
					</tr>

					<tr>
						<td align="center"><c:out value="${message.id}"></c:out></td>
						<td align="center"><c:out value="${message.author}"></c:out></td>
						<td align="center" width="30%"><c:out
								value="${message.message}"></c:out></td>
						<td class="buttons"><a class="delete"
							href="<%=request.getContextPath()%>/deleteMessage/${message.id}.html">Delete</a></td>
						<td class="buttons"><a class="edit"
							href="<%=request.getContextPath()%>/messageBoard/${message.id}.html">Edit</a></td>
					</tr>

				</c:forEach>

			</table>

		</div>
	</div>

	<div class="editField">
		<form:form commandName="messageBean">
			<p class="message">Message:</p>
			<form:input path="message" size="50" maxlength="50" type="text"
				value="${messageBean.message}" placeholder="Type your message here"/>
		</form:form>
	</div>
	<br>

	<p class="logout">
		<a class="button" href="<%=request.getContextPath()%>/index.html">LOGOUT</a>
	</p>

</body>
</html>