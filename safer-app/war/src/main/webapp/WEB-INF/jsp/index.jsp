<%@page contentType="text/html; charset=UTF-8" errorPage="error.jsp"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Safer - Login </title>

 <link rel="stylesheet" href="<%=request.getContextPath()%>/style/common.css"/>

</head>
<body>

	<h1>LOGIN</h1>
	
<div class="loginFormContainer">
<div class="loginForm">
	
		<table class="table">
	<form:form commandName="loginBean">
		
		<tr>
		<td>username:</td>
		<td class="input" ><form:input type="password" path="username" maxlength="15" /></td>
		<tr>
		<td>password:</td>
		<td class="input" ><form:input type="password" path="password" maxlength="15" /></td>
		<tr>
		<td><img class="image" src="<%= request.getContextPath() %>/images/yubikeysmal.png" alt=""></td>	
		<td class="input" ><form:input class="otpInput" type="password" path="otp" maxlength="44" /></td>
		<td><input type="submit" style= "visibility:hidden;" /></td>
		


	</form:form>

		</table>
</div>
</div>

</body>
</html>