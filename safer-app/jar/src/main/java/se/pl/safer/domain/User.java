package se.pl.safer.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import se.pl.safer.utils.IdHolder;

/**
 * The user object contains all the necessary data to help identify a unique person.
 * The password are hashed and salted before it saved in DB. 
 * @author Team Safebroker
 */
@Entity
public class User implements IdHolder{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column(name = "username", unique=true)
	private String username;
	private String password;
	private String salt;
	private int clientId;
	private String publicId;
	

	public User() {}
	
	public User(String username, String password){
		setUsername(username);
		setPassword(password);
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}
	
	public String getSalt() {
		return salt;
	}
	
	public void setSalt(String salt) {
		this.salt = salt;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public String getPublicId() {
		return publicId;
	}

	public void setPublicId(String publicId) {
		this.publicId = publicId;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", password="
				+ password + ", salt=" + salt + ", clientId=" + clientId
				+ ", publicId=" + publicId + ", getUsername()=" + getUsername()
				+ ", getPassword()=" + getPassword() + ", getId()=" + getId()
				+ ", getSalt()=" + getSalt() + ", getClientId()="
				+ getClientId() + ", getPublicId()=" + getPublicId() + "]";
	}

	
	
}

