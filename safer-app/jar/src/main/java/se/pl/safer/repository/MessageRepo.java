package se.pl.safer.repository;

import java.util.List;
import se.pl.safer.domain.Message;

public interface MessageRepo extends BaseRepository<Message>{
	
	List<Message> getAllMessages();

}
