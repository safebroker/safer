package se.pl.safer.services;

import javax.ejb.Local;

import se.pl.safer.domain.User;

@Local
public interface YubikeyService {
	
	boolean loginWithYubiKey(String otp, User validUser);
	

}
