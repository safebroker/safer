package se.pl.safer.repository;

import java.util.List;

import javax.persistence.Query;

import se.pl.safer.domain.User;

public class JpaUserRepo extends JpaRepository<User> implements UserRepo {

	@SuppressWarnings("unchecked")
	@Override
	public User getUserWithUserName(String userName) {
		Query query = em.createQuery("select u from User u where u.username = ?1");
		query.setParameter(1, userName);
		
		List<User> resultList = query.getResultList();
		
		if (resultList.size() == 1) {
			User user = resultList.get(0);
			return user;
		} else if (resultList.isEmpty()) {
			return null;
		} else {
			throw new RuntimeException("Received two users for userName " + userName + ". This should not be possible.");
		}
		
	}
	
	
	


	
}
