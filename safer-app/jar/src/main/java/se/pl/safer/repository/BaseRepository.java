package se.pl.safer.repository;

import se.pl.safer.utils.IdHolder;

/**
 * The most basic methods of a repository. By having them in a separate
 * interface we can force other repository interfaces to implement these
 * methods.
 */
public interface BaseRepository<E extends IdHolder> {

	long persist(E entity);

	void remove(E entity);

	E findById(long id);

	void update(E entity);

}