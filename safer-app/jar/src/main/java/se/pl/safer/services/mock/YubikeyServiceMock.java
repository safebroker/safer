package se.pl.safer.services.mock;

import javax.ejb.Local;

import se.pl.safer.domain.User;

@Local
public interface YubikeyServiceMock {
	
	boolean loginWithYubiKey(String otp, User validUser);
	

}
