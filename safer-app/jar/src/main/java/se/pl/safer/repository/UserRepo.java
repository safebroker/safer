package se.pl.safer.repository;

import se.pl.safer.domain.User;

public interface UserRepo extends BaseRepository<User>{

	User getUserWithUserName(String userName);
	
	
}
