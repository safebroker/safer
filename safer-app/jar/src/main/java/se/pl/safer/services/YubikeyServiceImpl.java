package se.pl.safer.services;

import java.util.logging.Logger;

import javax.ejb.Stateless;

import se.pl.safer.domain.User;

import com.yubico.client.v2.YubicoClient;
import com.yubico.client.v2.YubicoResponse;
import com.yubico.client.v2.YubicoResponseStatus;
import com.yubico.client.v2.exceptions.YubicoValidationException;
import com.yubico.client.v2.exceptions.YubicoValidationFailure;

/**
 * A Service that handles all the logic for the yubikey function.
 * YubicoClient are a lib that is provided inside this project. 
 * It's provides the connection to the Yubicloud.
 * @author Team Safebroker
 */
@Stateless
public class YubikeyServiceImpl implements YubikeyService{

	private static Logger log = Logger.getLogger(YubikeyServiceImpl.class.getName());

	@Override
	public boolean loginWithYubiKey(String otp, User validUser) {

		YubicoClient client = null;
		YubicoResponse response = null;
		String publicId;

		try {
			
			publicId = YubicoClient.getPublicId(otp);
			client = YubicoClient.getClient(validUser.getClientId());
			response = client.verify(otp);

			if(validateOtpAndClientId(publicId, validUser)) {
			
				log.info("Log in with yubikey = Success");
				return response.getStatus() == YubicoResponseStatus.OK;
			
			} else {
				log.info("Log in with yubikey = Fail");
			}

		} catch (IllegalArgumentException ia) {
			log.info("Yubikey not valid");
		} catch (YubicoValidationException e) {
			log.info("Yubikey not valid");
		} catch (YubicoValidationFailure e) {
			log.info("Yubikey not valid");
		}
		return false;
	}

	private boolean validateOtpAndClientId(String publicId,User validUser) {
		return publicId.contains(validUser.getPublicId());
	}
}
