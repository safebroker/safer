package se.pl.safer.domain.mock;

import java.util.ArrayList;
import java.util.List;

import se.pl.safer.domain.Message;

public class MockData {
	
	private List<Message> messages = new ArrayList<>();
	
	public MockData(){
		messages.add(new Message(1l, "Mocked message 1"));
		messages.add(new Message(2l, "Mocked message 2"));
		messages.add(new Message(3l, "Mocked message 3"));
		messages.add(new Message(4l, "Mocked message 4"));
		
	}


	public List<Message> getMessages() {
		return messages;
	}


	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}


	@Override
	public String toString() {
		return "MockData [messages=" + messages + "]";
	}
	

}
