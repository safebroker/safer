package se.pl.safer.services;

import java.util.List;

import javax.ejb.Local;

import se.pl.safer.domain.Message;

@Local
public interface MessageService {

	Message getMessage(long id);

	long createMessage(Message message);
	
	void removeMessage(long id);
	
	void updateMessage(Message message);
	
	List<Message> getAllMessages();
	
	boolean isMessageInDb(long id);
}
