package se.pl.safer.services;

import javax.ejb.Local;

import se.pl.safer.domain.User;

@Local
public interface UserService {

	boolean userIsValid(String username, String password);
	
	User getValidUser();
	
}
