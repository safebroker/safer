package se.pl.safer.services.mock;

import java.util.logging.Logger;

/**
 * A mocked class to skip the validation against Yubico Servers
 * 
 * @author Team Safebroker
 */

import javax.ejb.Stateless;

import se.pl.safer.domain.User;

@Stateless
public class YubikeyServiceImplMock implements YubikeyServiceMock{
	
	private static Logger log = Logger.getLogger(YubikeyServiceImplMock.class.getName());

	@Override
	public boolean loginWithYubiKey(String otp, User validUser) {
		log.info("Using the mocked implementation of Yubikey. No check is done!!");
			return true;

	}

}
