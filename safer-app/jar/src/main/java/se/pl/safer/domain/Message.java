package se.pl.safer.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import se.pl.safer.utils.IdHolder;

/**
 * Message is an entity that can be saved in DB. 
 * The "author" are the same as current "authorizedUSERNAME". 
 * @author Team Safebroker
 */
@Entity
public class Message implements IdHolder{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String message;
	private String author;

	public Message() {}
	
	public Message(long id, String message){
		setId(id);
		setMessage(message);
	}

	public Message(String message) {
		setMessage(message);		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	@Override
	public String toString() {
		return "Message [id=" + id + ", message=" + message + ", author="
				+ author + ", getId()=" + getId() + ", getMessage()="
				+ getMessage() + ", getAuthor()=" + getAuthor() + "]";
	}

}
