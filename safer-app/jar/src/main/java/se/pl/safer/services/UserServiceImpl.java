package se.pl.safer.services;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.pl.safer.domain.User;
import se.pl.safer.repository.UserRepo;
import se.pl.safer.utils.PasswordHelper;

/**
 * A Service for the users. No CRUD.
 * Runs methods for hash and salt.
 * Contains methods for generate new salt and hash when "generatingSaltMODE = true;"
 * @Inject UserRepo
 * @author Team Safebroker
 */
@Stateless
public class UserServiceImpl implements UserService{

	private static Logger log = Logger.getLogger(UserServiceImpl.class.getName());

	@Inject
	private UserRepo userRepo;

	PasswordHelper pass = new PasswordHelper();
	private User userDb = null;
	// Change this to true to generate salt and hash. Values will be found in the log.
	private static boolean generatingSaltMODE = false;

	@Override
	public boolean userIsValid(String username, String password) {
		String generateHashWithKnownSalt = null;

		//// Only when generatingSaltMODE is true //// 
		if(generatingSaltMODE)
			runOnlyToGenerateSaltAndHash(password);
		//// Only when generatingSaltMODE is true ////

		userDb = getUserRepo().getUserWithUserName(username);

		if(userDb == null) {
			log.info("User don't exist in DB");
			return false;
		}
		try {
			generateHashWithKnownSalt = pass.generateHashWithKnownSalt(password, userDb.getSalt());
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			e.printStackTrace();
		}

		if(generateHashWithKnownSalt.contentEquals(userDb.getPassword())) {
			log.info("Hashed passwords have a perfect match! The login procedure will continue with validating the yubikey");
			return true;	
		}
		log.info("Hashed passwords don't match! The login procedure is canceled.");
		return false;
	}

	// return ValidUser
	@Override
	public User getValidUser() {
		return userDb;
	}

	// Only for password administration. Not for production.
	private void runOnlyToGenerateSaltAndHash(String password) {
		byte[] salt = pass.generateSalt();
		String saltString = PasswordHelper.toHex(salt);
		log.info("Generated new Salt = " + saltString);
		try {
			log.info("Generated Hash = "+pass.generateHashWithNewSalt(password, salt));
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			e.printStackTrace();
		}
	}

	public UserRepo getUserRepo() {
		return userRepo;
	}
	public void setUserRepo(UserRepo userRepo) {
		this.userRepo = userRepo;
	}
}