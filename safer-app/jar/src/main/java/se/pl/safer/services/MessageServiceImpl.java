package se.pl.safer.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.pl.safer.domain.Message;
import se.pl.safer.repository.MessageRepo;

/**
 * A service to handle the created messages in the app "Safer". Full CRUD.
 * @Inject MessageRepo 
 * @author Team Safebroker
 */
@Stateless
public class MessageServiceImpl implements MessageService {

	@Inject
	MessageRepo repo;

	@Override
	public Message getMessage(long id) {
		return getRepo().findById(id);
	}

	@Override
	public long createMessage(Message message) {
		return getRepo().persist(message);
	}

	@Override
	public void removeMessage(long id) {
		Message message = getMessage(id);
		getRepo().remove(message);
	}

	@Override
	public void updateMessage(Message message) {
		getRepo().update(message);
	}

	@Override
	public List<Message> getAllMessages() {
		return repo.getAllMessages();
	}

	@Override
	public boolean isMessageInDb(long id) {
		Message message = repo.findById(id);
		return message != null;
	}
	
	public MessageRepo getRepo() {
		return repo;
	}
	
	public void setRepo(MessageRepo repo) {
		this.repo = repo;
	}

}
