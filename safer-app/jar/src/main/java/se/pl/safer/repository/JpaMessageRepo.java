package se.pl.safer.repository;

import java.util.List;
import org.apache.log4j.Logger;
import se.pl.safer.domain.Message;

public class JpaMessageRepo extends JpaRepository<Message> implements MessageRepo {

	Logger log = Logger.getLogger(JpaMessageRepo.class.getSimpleName());

//	private MockData mockData;

	@SuppressWarnings("unchecked")
	@Override
	public List<Message> getAllMessages() {
		return em.createQuery("select m from Message m").getResultList();
	}

}
