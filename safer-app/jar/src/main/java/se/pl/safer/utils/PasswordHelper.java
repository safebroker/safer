package se.pl.safer.utils;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

public class PasswordHelper {

	private static final String HASH_ALGORITHM = "PBKDF2WithHmacSHA1";
	private static final String SALT_ALGORITHM = "SHA1PRNG";
	public static final int HASH_BYTE_SIZE = 24;
	public static final int PBKDF2_ITERATIONS = 1000;

	public String generateHashWithKnownSalt(String password, String salt) 
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		return generateHash(password.toCharArray(), salt.getBytes());
	}

	public String generateHashWithNewSalt(String password, byte[] salt) 
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		return generateHash(password.toCharArray(), salt);
	}

	private String generateHash(char[] password, byte[] salt)
			throws NoSuchAlgorithmException, InvalidKeySpecException
			{

		// Hash the password
		byte[] hash = pbkdf2(password, salt, PBKDF2_ITERATIONS, HASH_BYTE_SIZE);
		// format iterations:salt:hash
		return PBKDF2_ITERATIONS + ":" + toHex(salt) + ":" +  toHex(hash);
			}


	public byte[] generateSalt() {
		byte[] salt = new byte[24];
		SecureRandom random = new SecureRandom(SALT_ALGORITHM.getBytes());
		random.nextBytes(salt);
		return salt;
	}

	private static byte[] pbkdf2(char[] password, byte[] salt, int iterations, int bytes)
			throws NoSuchAlgorithmException, InvalidKeySpecException
			{
		PBEKeySpec spec = new PBEKeySpec(password, salt, iterations, bytes * 8);
		SecretKeyFactory skf = SecretKeyFactory.getInstance(HASH_ALGORITHM);
		return skf.generateSecret(spec).getEncoded();
			}

	@SuppressWarnings("unused")
	private static byte[] fromHex(String hex)
	{
		byte[] binary = new byte[hex.length() / 2];
		for(int i = 0; i < binary.length; i++)
		{
			binary[i] = (byte)Integer.parseInt(hex.substring(2*i, 2*i+2), 16);
		}
		return binary;
	}

	/**
	 * Converts a byte array into a hexadecimal string.
	 * @param   array       the byte array to convert
	 * @return              a length*2 character string encoding the byte array
	 */
	public static String toHex(byte[] array)
	{
		BigInteger bi = new BigInteger(1, array);
		String hex = bi.toString(16);
		int paddingLength = (array.length * 2) - hex.length();
		if(paddingLength > 0) 
			return String.format("%0" + paddingLength + "d", 0) + hex;
		else
			return hex;
	}

}
