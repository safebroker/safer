# SAFER - "Better Safe than broke"

##Produkter

### Safer Webapp
Applikationen körs på lokal server i webbläsaren där inloggning hanteras genom att dels söka efter användaren i databasen (MYSQL) och genom rätt YubicoKey-nyckel mot Yubicos API.

### Safer Backend (Server)
Applikationen använder [Springframework](http://projects.spring.io/spring-framework/) och [Maven](http://maven.apache.org/) på en JBoss server som i aktuell version körs via localhost (http://localhost:8080/safer-web/) eller localhost genom https: (https://localhost:8443/safer-web/). I applikationen medföljer en [Yubico java-client](http://yubico.github.io/yubico-java-client/)

## BEROENDEN
För att köra applikationen krävs följande:

** Klient **

* WEBLÄSARE(applikationen testad på Google Chrome/Safari/Firefox)

* JAVA EE

* Eclipse Enterprise Edition 

* MAVEN

* INTERNETUPPKOPPLING

** Server och Databas **

* JBOSS (applikationen testad på v7.1)

* MYSQL

***

##INSTALLATION
För att köra applikationen klona ner repot från master_branch. Ladda ner safer.xml från downloads och placera i JBoss \standalone\configuration mappen.
Importera safer-app och yubico-java-client-master som existing maven project i Ecplise och ange safer.xml som configuration file för JBoss Runtime Server.
I safer.xml rad 255 har du inställningarna för HTTPS eller localhost, följ instruktionerna. Skapa ett schema i SQL som heter safer.
Starta därefter JBoss Servern och i root-mappen för safer-app kör mvn clean install jboss-as:deploy. 

Därefter fyller du i nedanstående värden i ditt SQL schema under tables > user, eller se över metoderna i user.java för att sätta upp egna användare.

Paul: 

* Yubikey clientId = 19673	

* PublicId = ccccccdhhfrk

* username = Paul

* Password = safebroker

* Password# = 
1000:323232313136383332356630666665636431333761633330353561326361613331393732396438613661623431343261:26b0392e1850450f1b20e7968b8ec5fd9a64b4572dc41078

* Salt = 2221168325f0ffecd137ac3055a2caa319729d8a6ab4142a


Christian: 

* Yubikey clientId = 19673
	
* PublicId = ccccccdhhfrk

* username = Christian

* Password = kollekotten

* Password# = 1000:393934353933613165323762343832666136333335346131373333333162643364633539616233376237356439633963:7044e11564f92a6eab6f0bbe35831f488f2750561f515ea0

* Salt = 994593a1e27b482fa63354a173331bd3dc59ab37b75d9c9c

Johan: 

* Yubikey clientId = 19678
	
* PublicId = vvjhfujbheei

* username = Johan

* Password = maltmaniac

* Password# = 1000:663037646632633331346165636234646232616336316331666239616461646234653137343362613539343862306638:eafdaf7a65ddbfc0601eed40e1497d041f887f584ba6a680

* Salt = f07df2c314aecb4db2ac61c1fb9adadb4e1743ba5948b0f8

##---- UBUNTU SERVER ----
(Om vi vill köra den. I nuläget gör vi det inte.)

LOGGA IN PÅ DIN DROPLET:
KÖR I EGEN ROOT/HEMMAP: ssh root@128.199.36.235
ANGE LÖSEN: XXXXXXXXXXX

NAVIGERA TILL RÄTT MAPP (BIN-mappen i Jboss).
STÄLL DIG I ROOT MED: cd /

SKRIV: cd /usr/local/share/jboss/bin

SÄTT UPP ETT SCHEMA MED NAMN "SAFER" PÅ SERVERNS DATABAS.

(safer.xml gigger i repot)
STARTA SERVER:
SKRIV: ./standalone.sh -c safer.xml -Djboss.bind.address=0.0.0.0 -Djboss.bind.address.management=0.0.0.0&
(startar servern med safer.xml)

AVSLUTA JBOSS:
SKRIV: ./jboss-cli.sh --connect --controller=0.0.0.0:9990 command=:shutdown

LOGGA UT SSH: (servern lever vidare)
SKRIV: exit

AVSLUTA ALLT:
SKRIV: sudo reboot 
***
# Changelog

###Sprint 2

* Logga in med YubiKey(2 användare)

* Full CRUD på meddelanden

* Logga ut(nollställa sessionen)

* Rensat ut onödiga klasser och metoder

* Snyggare gränssnitt på jsp-sidorna

###Sprint 1

* Satt upp en lokal servermiljö

* Satt upp serviceklasser för meddelanden/user/yubikey

* Konfigurerat MYSQL för användarvärden med SALT och HASH och clientId från yubikey

* Skapat jsp-sidor för log-in och meddelanden

* Konfigurerat lokal server

* Satt upp Maven miljö

***

För en mer utförlig beskrivning och logg läs gärna vår [Tekniska Rapport](https://docs.google.com/document/d/1lMpySWmZowOBJ18yO0YOq7TafCzDwCR43fKWsLrjGl0/edit?usp=sharing)

# Utvecklare

* [Christian Karlsson](https://bitbucket.org/Somnium)

* [Paul Lachenardière](https://bitbucket.org/paullachenardiere)

* [Johan Lindgren](https://bitbucket.org/JohanLindgren)